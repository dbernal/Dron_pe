package com.s4n.main;

import org.junit.Test;

public class MainTest {


    @Test
    public void mainWithRouteParameterOkTest() throws Exception {
        final String[] strings = {"C:\\apps\\dron"};

        Main.main(strings);
    }

    @Test
    public void mainWithRouteParameterNullTest(){

        final String[] strings = {};
        Main.main(strings);
    }
}