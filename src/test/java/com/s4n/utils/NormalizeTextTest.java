package com.s4n.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NormalizeTextTest {

    private NormalizeText normalizeText;

    @Before
    public void setUp() throws Exception {
        normalizeText = new NormalizeText();
    }

    @Test
    public void testNormalizeCoordinates() throws Exception{

        final String coordinates = "AAADAIIAA";

        final String normalizeCoordinates = normalizeText.normalizeCoordinates(coordinates);

        Assert.assertNotNull(normalizeCoordinates);
        Assert.assertEquals(normalizeCoordinates, "N" + coordinates);
    }

    @Test(expected = Exception.class)
    public void isInvalidTextTest() throws Exception {

        normalizeText.isInvalidText("AAAAAIIDAAA)(!'#");
    }
}