package com.s4n.file;

import com.s4n.dto.Coordinates;
import com.s4n.dto.Direction;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WriteDeliveriesTest {

    @InjectMocks
    private WriteDeliveries writeDeliveries;

    private static final String ROUTE = "C:\\apps";

    @Before
    public void testWriter() {
        writeDeliveries = new WriteDeliveries();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void generateOutputFileTest() throws Exception {

        final List<Coordinates> coordinatesList = new ArrayList<Coordinates>();

        final Map<String, Direction> directionMap = new HashMap<String, Direction>();

        final Direction direction = new Direction();
        direction.setY(4);
        direction.setX(2);

        directionMap.put("AAIADAD", direction);

        final Coordinates coordinates = new Coordinates();
        coordinates.setOrientation(directionMap);

        coordinatesList.add(coordinates);

        writeDeliveries.writer(coordinatesList, ROUTE);
    }

    @Test(expected = Exception.class)
    public void generateOutputFileRouteNullTest() throws Exception {
        writeDeliveries.writer(new ArrayList<Coordinates>(), null);

    }

    @Test(expected = Exception.class)
    public void generateOutputFileCoordinatesNullTest() throws Exception {
        writeDeliveries.writer(null, ROUTE);
    }
}