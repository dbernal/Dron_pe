package com.s4n.utils;

/**
 * Created by Entelgy on 08/08/2016.
 */
public enum EnumDirection {

    NORTH("Dirección Norte"),
    SOUTH("Dirección Sur"),
    EAST("Dirección Oriente"),
    WEST("Dirección Occidente");

    private String value;

    private EnumDirection(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
