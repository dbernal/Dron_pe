package com.s4n.dto;

import com.s4n.utils.EnumDirection;

/**
 * Created by Entelgy on 08/08/2016.
 */
public class Direction {

    private EnumDirection direction = EnumDirection.NORTH;
    private Integer orientation = new Integer(1);
    private Integer x = new Integer(0);
    private Integer y = new Integer(0);

    public Direction() {
    }

    public EnumDirection getDirection() {
        return direction;
    }

    public void setDirection(EnumDirection direction) {
        this.direction = direction;
    }

    public Integer getOrientation() {
        return orientation;
    }

    public void setOrientation(Integer orientation) {
        this.orientation = orientation;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = this.x + x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = this.y + y;
    }
}
