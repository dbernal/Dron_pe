package com.s4n.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Entelgy on 08/08/2016.
 */
public class Coordinates {

    private Map<String, Direction> orientation = new HashMap<String, Direction>();

    public Coordinates() {
    }

    public Map<String, Direction> getOrientation() {
        return orientation;
    }

    public void setOrientation(Map<String, Direction> orientation) {
        this.orientation = orientation;
    }
}
