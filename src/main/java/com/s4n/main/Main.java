package com.s4n.main;

import com.s4n.dto.Coordinates;
import com.s4n.file.EvaluatorOrder;
import com.s4n.file.WriteDeliveries;
import com.s4n.utils.Constant;

import java.util.List;

/**
 * Created by Entelgy on 08/08/2016.
 */
public class Main {

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println(Constant.EMPTY_PARAMETERS);

        } else {
            final String route = args[0];

            List<Coordinates> coordinates = null;
            try {
                coordinates = new EvaluatorOrder().getCoordinates(route);
                new WriteDeliveries().writer(coordinates, route);
            } catch (final Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
